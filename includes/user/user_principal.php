<?php 


?>
<div class="content">
    <div class="animated fideIn">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">1500,00</h3>
                            <p class="text-light mt-1 m-0">Saldo USD</p>
                        </div>
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-cart">
                                
                            </i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">USD 0</h3>
                            <p class="text-light mt-1 m-0">Disponible USD </p>
                        </div>
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-cart">
                                
                            </i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">2.500.000</h3>
                            <p class="text-light mt-1 m-0">Saldo CLP</p>
                        </div>
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-cart">
                                
                            </i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">0</h3>
                            <p class="text-light mt-1 m-0">Disponible CLP</p>
                        </div>
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-cart">
                                
                            </i>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


